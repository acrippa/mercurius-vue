module.exports = {
    configureWebpack: {
        resolve: {
            extensions: ['.wasm', '.mjs', '.js', '.json'],
        },
    },

    pwa: {
        name: 'Mercurius',
    },

    lintOnSave: undefined,
}
