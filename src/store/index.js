import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import asset from './asset'
import authentication from './authentication'
import campaign from './campaign'
import list from './list'
import template from './template'
import user from './user'

export default new Vuex.Store({
    modules: {
        asset,
        authentication,
        campaign,
        list,
        template,
        user,
    },
})
