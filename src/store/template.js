import alert from '@/common/alert'
import { templateAPI } from '@/api'
import router from '@/router'

export default {
    namespaced: true,
    state: {
        templates: [],
        template: {},
    },
    actions: {
        templates: ({ commit }) => {
            templateAPI.templates().then(
                response => {
                    commit('templates', response.data)
                },
                error => alert.error(error)
                )
        },
        template: ({ commit }, id) => {
            templateAPI.template(id).then(
                response => {
                    commit('template', response.data)
                },
                error => alert.error(error)
                )
        },
        preview: (contect, id) => {
            templateAPI.preview(id).then(
                response => response.data,
                error => '')
        },
        save({ commit, state }, template) {
            //template = _.assign({}, state.template, template)
            templateAPI.save(template).then(
                response => {
                    commit('template', response.data)
                    router.push('/template/' + state.template.id)
                },
                error => alert.error(error)
                )
        },
        test: (context, template) => {
            templateAPI.test(template).then(
                response => {
                    resolve()
                },
                error => alert.error(error)
                )
        },
    },
    mutations: {
        templates: (state, templates) => (state.templates = templates),
        template: (state, template) => (state.template = template),
    },
}
