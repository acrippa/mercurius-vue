import { authAPI } from '@/api'

const user = JSON.parse(localStorage.getItem('user'))
const initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null }

export default {
    namespaced: true,
    state: initialState,
    actions: {
        login: ({ commit }, { email, password }) => {
            return new Promise((resolve, reject) => {
                authAPI.login({ email: email, password: password }).then(
                    user => {
                        commit('login', user.data)
                        resolve()
                    },
                    error => reject(error)
                )
            })
        },
        logout: ({ commit }) => {
            return new Promise(resolve => {
                authAPI.logout().then(() => {
                    commit('logout')
                    resolve()
                })
            })
        },
    },
    mutations: {
        login: (state, user) => {
            state.status.loggenIn = true
            state.user = user
            localStorage.setItem('user', JSON.stringify(user))
            window.axios.defaults.headers.common['Authorization'] =
                'Bearer ' + user.access_token
        },
        logout: state => {
            state.status = {}
            state.user = null
            localStorage.removeItem('user')
        },
    },
}
