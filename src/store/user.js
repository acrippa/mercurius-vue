import { userAPI } from '@/api'

export default {
    namespaced: true,
    state: {
        users: {},
        user: {},
    },
    actions: {
        users: ({ commit }) => {
            return new Promise((resolve, reject) => {
                userAPI.users().then(
                    response => {
                        commit('users', response.data)
                        resolve()
                    },
                    error => reject(error)
                )
            })
        },
    },
    mutations: {
        users: (state, users) => (state.users = users),
    },
}
