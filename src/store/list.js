import alert from '@/common/alert'
import { listAPI } from '@/api'
import router from '@/router'

export default {
    namespaced: true,
    state: {
        lists: [],
        list: {},
    },
    actions: {
        lists: ({ commit }) => {
            listAPI.lists().then(
                response => {
                    commit('lists', response.data)
                },
                error => alert.error(error)
                )
        },
        list: ({ commit }, id) => {
            listAPI.list(id).then(
                response => {
                    commit('list', response.data)
                },
                error => alert.error(error)
                )
        },
        save({ commit, state }) {
            listAPI.save(state.list).then(
                response => {
                    commit('list', response.data)
                    router.push('/list/' + state.list.id)
                },
                error => alert.error(error)
                )
        },
    },
    mutations: {
        lists: (state, lists) => (state.lists = lists),
        list: (state, list) => (state.list = list),
    },
}
