import alert from '@/common/alert'
import { campaignAPI } from '@/api'
import router from '@/router'

export default {
    namespaced: true,
    state: {
        campaigns: [],
        campaign: {},
    },
    actions: {
        campaigns: ({ commit }) => {
            campaignAPI.campaigns().then(
                response => {
                    commit('campaigns', response.data)
                },
                error => alert.error(error)
                )
        },
        campaign: ({ commit }, id) => {
            campaignAPI.campaign(id).then(
                response => {
                    commit('campaign', response.data)
                },
                error => alert.error(error)
                )
        },
        save({ commit, state }) {
            campaignAPI.save(state.campaign).then(
                response => {
                    commit('campaign', response.data)
                    router.push('/campaigns/' + state.campaign.id)
                },
                error => alert.error(error)
                )
        },
    },
    mutations: {
        campaigns: (state, campaigns) => (state.campaigns = campaigns),
        campaign: (state, campaign) => (state.campaign = campaign),
    },
}
