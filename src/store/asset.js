import alert from '@/common/alert'
import { assetAPI } from '@/api'

export default {
    namespaced: true,
    state: {
        assets: [],
    },
    actions: {
        assets: ({ commit }) => {
            assetAPI.assets().then(
                response => {
                    commit('assets', response.data)
                },
                error => alert.error(error)
                )
        },
    },
    mutations: {
        assets: (state, assets) => (state.assets = assets),
    },
}
