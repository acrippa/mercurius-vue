import axios from 'axios'

window.axios = axios
let user = JSON.parse(localStorage.getItem('user'))
let bearer = user && user.access_token ? user.access_token : false
if (bearer) {
    window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + bearer
}
