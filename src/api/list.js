import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/lists'

export const listAPI = {
    lists: () => window.axios.get(API_URL),
    list: id => window.axios.get(API_URL + '/' + id),
    save: list => {
        if (list.id) {
            return window.axios.put(API_URL + '/' + list.id, list)
        } else {
            return window.axios.post(API_URL, list)
        }
    },
    delete: list => window.axios.delete(API_URL + '/' + list.id),
}
