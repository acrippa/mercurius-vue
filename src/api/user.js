import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/users'

export const userAPI = {
    users: () => window.axios.get(API_URL),
}
