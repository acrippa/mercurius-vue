import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/campaigns'

export const campaignAPI = {
    campaigns: () => window.axios.get(API_URL),
    campaign: id => window.axios.get(API_URL + '/' + id),
    save: campaign => {
        if (campaign.id) {
            return window.axios.put(API_URL + '/' + campaign.id, campaign)
        } else {
            return window.axios.post(API_URL, campaign)
        }
    },
    delete: campaign => window.axios.delete(API_URL + '/' + campaign.id),
}
