import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/auth'

export const authAPI = {
    login: login => window.window.axios.post(API_URL, login),
    logout: () => window.window.axios.get(API_URL + '/logout'),
}
