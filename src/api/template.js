import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/templates'

export const templateAPI = {
    templates: () => window.axios.get(API_URL),
    template: id => window.axios.get(API_URL + '/' + id),
    preview: id => window.axios.get(API_URL + '/' + id + '/preview'),
    thumbnail: id => API_URL + '/' + id + '/thumbnail',
    save: template => {
        if (template.id) {
            return window.axios.put(API_URL + '/' + template.id, template)
        } else {
            return window.axios.post(API_URL, template)
        }
    },
    delete: template => window.axios.delete(API_URL + '/' + template.id),
    test: template => window.axios.post(API_URL + '/send', template),
}
