import { MERCURIUS_CONFIG } from '@/config'

var API_URL = MERCURIUS_CONFIG.API_URL + '/assets'

export const assetAPI = {
    upload: MERCURIUS_CONFIG.API_URL + '/assets/upload',
    assets: () => window.axios.get(API_URL),
}
