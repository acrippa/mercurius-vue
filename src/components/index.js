import Vue from 'vue'
import Navigation from './Navigation'
import Snackbar from './Snackbar'

const components = {}

components.install = Vue => {
    Vue.component('navigation', Navigation)
    Vue.component('snackbar', Snackbar)
}

Vue.use(components)
