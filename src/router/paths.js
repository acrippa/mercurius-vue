export default [
    {
        path: '/login',
        name: 'Login',
        component: () => import(`@/views/Login.vue`),
    },
    {
        path: '/',
        meta: { requiresAuth: true },
        name: 'Mercurius',
        component: () => import(`@/views/Mercurius.vue`),
        children: [
            {
                path: 'dashboard',
                meta: { requiresAuth: true },
                name: 'Dashboard',
                component: () => import(`@/pages/Dashboard`),
            },

            {
                path: 'campaigns',
                meta: { requiresAuth: true },
                name: 'Campaigns',
                component: () => import(`@/pages/campaign/Campaigns`),
            },
            {
                path: 'campaign',
                meta: { requiresAuth: true },
                name: 'New Campaign',
                component: () => import(`@/pages/campaign/Campaign`),
            },
            {
                path: 'campaign/:id',
                meta: { requiresAuth: true },
                name: 'Campaign',
                component: () => import(`@/pages/campaign/Campaign`),
                props: true,
            },
            {
                path: 'templates',
                meta: { requiresAuth: true },
                name: 'Templates',
                component: () => import(`@/pages/template/Templates`),
            },
            {
                path: 'template',
                meta: { requiresAuth: true },
                name: 'New Template',
                component: () => import(`@/pages/template/Template`),
            },
            {
                path: 'template/:id',
                meta: { requiresAuth: true },
                name: 'Template',
                component: () => import(`@/pages/template/Template`),
                props: true,
            },
        ],
    },
    { path: '*', redirect: '/' },
]
