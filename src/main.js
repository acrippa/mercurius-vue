import Vue from 'vue'
import Event from 'pubsub-js'
import '@/plugins'
import '@/components'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import '@/registerServiceWorker'

Vue.prototype.$event = Event
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
